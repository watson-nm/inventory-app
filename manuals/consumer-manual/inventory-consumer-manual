\documentclass[a4paper,10pt]{article}

\usepackage{ucs}
\usepackage[utf8]{inputenc}
\usepackage{babel}
\usepackage{fontenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage[export]{adjustbox}
\usepackage[hidelinks]{hyperref}
\usepackage[margin=0.6in]{geometry}
\usepackage{tabularx}
\usepackage{arev}
\usepackage[T1]{fontenc}

\author{Nola Watson}
\title{Inventory Database Manager: Consumer Manual}

\begin{document}
\maketitle

\tableofcontents
\newpage

\addcontentsline{toc}{section}{Database Structure}
\section*{Database Structure}
This application is designed to be an easy to use and navigate front end for an inventory database.\\
This database contains two connected tables: \textbf{devices} and \textbf{assignees}.

\addcontentsline{toc}{subsection}{Devices}
\subsection*{Devices}
The devices table contains most of the information in the database.\\
The devices table contians ten columns.

\begin{table}[h]
    \small
        \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
        \hline
        \footnotesize
        ID & Assignee & Location & Asset \# & Serial \# & Device Type & Make & Model & Assign Date & Update Date\\
        \hline
        \end{tabular}
\end{table}

\addcontentsline{toc}{subsubsection}{Devices Column Data}
\subsubsection*{Devices Column Data}
\begin{itemize}
    \item
    \textbf{ID} - An automatically assigned unique id which identifies an item in a table.

    \item
    \textbf{Assignee} - The person, section, or location that a device is assigned to. In the database this is a numerical value matching an ID from the assignees table.

    \item
    \textbf{Location} - The physical location of a device; i.e. a cubicle or room number.

    \item
    \textbf{Asset \#} - The asset number assigned to a device.

    \item
    \textbf{Serial \#} - The serial number or service tag of a device; this is a unique value.

    \item
    \textbf{Device Type} - The type of a device (laptop, printer, monitor, etc.).

    \item
    \textbf{Make} - The make of a device.

    \item
    \textbf{Model} - The model of a device.

    \item
    \textbf{Assign Date} - The date when a device was given to an assignee.

    \item
    \textbf{Update Date} - The date when a table item was last updated.

\end{itemize}

\addcontentsline{toc}{subsection}{Assignees}
\subsection*{Assignees}
The assignees table contains the names of assigneees and, if applicaple, the sections they belong to.\\
There are only three columns in the assignees table.\\
\begin{table}[h]
    \centering
        \begin{tabular}{|c|c|c|}
        \hline
        ID & Name & Section \\
        \hline
        \end{tabular}
\end{table}

\addcontentsline{toc}{subsubsection}{Assignees Column Data}
\subsubsection*{Assignees Column Data}
\begin{itemize}
    \item
    \textbf{ID} - An automatically assigned unique id which identifies an item in a table. This is a numerical value linked to the assignees column of the devices table.

    \item
    \textbf{Name} - A person, section, or location.

    \item
    \textbf{Section} - The section an assignee belongs to; may help when differentiating assignee's with the same name.

\end{itemize}
\newpage

\addcontentsline{toc}{section}{Navigation}
\section*{Navigation}
\paragraph{} When first opening the inventory application users are greeted with the start page. Selecting the button labeled "Continue to Inventory" will bring you to the devices page.
\addcontentsline{toc}{subsection}{Devices}
\subsection*{Devices}
\paragraph{} The device inventory section of the application displays the contents of the devices table, broken up into 50 results per page. There is a navigation bar at the bottom of the page which you can use to see more table items.\\

\paragraph{} Above the table you see on the page, there is:
\begin{itemize}
    \item
    A search bar

    \item
    A "Create Entry" button

    \item
    A button that brings you to a page where you can see a list of assignees

    \item
    A button that allows you to see every device on a single page

    \item
    An export button

    \item
    An import button attached to a file selector
\end{itemize}

\addcontentsline{toc}{subsection}{Assignees}
\subsection*{Assignees}


\newpage

\addcontentsline{toc}{section}{Searching}
\section*{Searching}
\includegraphics[width=14cm]{searchbar}

The search bar can be located at the top of the Devices, Assignees, and Search pages. The leftmost portion of the search bar is a dropdown menu, the center is a texbox where a user can enter a search term, and the rightmost portion is a button which will submit the query.\\

It is possible to perform a search using an incomplete search term. For example you may type the search term "Doe" when looking for all devices assigned to "Jane Doe".\\
\includegraphics[width=16cm]{search-exa}

\addcontentsline{toc}{subsection}{Devices Search}
\subsection*{Devices Search}

\begin{minipage}[t]{0.75\textwidth}
    The dropdown menu displays a list of searchable categories.

\begin{itemize}
    \item
    \textbf{ID} - Search for an item by using its unique table ID.

    \item
    \textbf{Assigned To} - Search for all items with a specific assignee.

    \item
    \textbf{Location} - Search for all items in a specif location.

    \item
    \textbf{Asset \#} - Search for an item by using its asset number.

    \item
    \textbf{Serial \#} - Search for an item by using its serial number/service tag.

    \item
    \textbf{Device} - Search for all instances of a certain device type.

    \item
    \textbf{Make} - Seach for all items of a certain make.

    \item
    \textbf{Model} - Search for all items of a certain model.

    \item
    \textbf{Assign Date} - Search for items by the date when they were assigned.

    \item
    \textbf{Update Date} - Search for items by when their entry was last updated.
\end{itemize}
When a category has been chosen and a serch term typed into the text box select the Search button to be taken to a page with the search results.
\end{minipage}
\hfill
\begin{minipage}[t]{0.5\textwidth}
    \includegraphics[width=0.45\textwidth, valign=t]{search-dropdown-d}
\end{minipage}


\addcontentsline{toc}{subsection}{Assignees Search}
\subsection*{Assignees Search}

\begin{minipage}[t]{0.8\textwidth}
The assignees dropdown only contians three categories.
\begin{itemize}
    \item
    \textbf{ID} - Search for an assignee using a unique table ID.

    \item
    \textbf{Name} - Search for an assignee by name.

    \item
    \textbf{Section} - Search for any assignees in a particular section, if applicaple.
\end{itemize}
As with the devices search when a category has been chosen and a search term supplied you can select the Search button to be brought to a page of search results.
\end{minipage}
\hfill
\begin{minipage}[t]{0.5\textwidth}
    \includegraphics[width=0.3\textwidth, valign=t]{search-dropdown-a}
\end{minipage}
\newpage

\addcontentsline{toc}{section}{Creating Items}
\section*{Creating Items}
You can add new entries to the devices and assignees tables through the use of the "Create Entry" button located below the search bar.

\addcontentsline{toc}{subsection}{Creating Devices}
\subsection*{Creating Devices}
When creating a new device the following input fields are available:
\begin{itemize}
    \item
    \textbf{Assignee Name} - A required field, the name of the person or section which the device is to be assigned.

    \item
    \textbf{Assignee Section} - If applicaple, the section to which an assignee belongs.

    \item
    \textbf{Location} - The physical location of the device, such as a cubicle or room number.

    \item
    \textbf{Asset Number} - The asset number given to the device.

    \item
    \textbf{Serial Number} - The serial number or service tag of the device.

    \item
    \textbf{Device Type} - The type of device. Laptop, printer, and monitor are all acceptable inputs.

    \item
    \textbf{Make} - The make of the device. Some examples would be: HP, DELL, and Lenovo.

    \item
    \textbf{Model} - The model of the device.

    \item
    \textbf{Assign Date} - The date on which the device is being assigned.
\end{itemize}

The page will provide sugestions for input based on data already in the table for: name, section, device type, make, and model.

\addcontentsline{toc}{subsection}{Creating Assignees}
\subsection*{Creating Assignees}
When creating a new assignee the following input fields are available:
\begin{itemize}
    \item
    \textbf{Name} - The full name of the assignee or section.

    \item
    \textbf{Section} - If applicaple, the section to which the assignee belongs.
\end{itemize}

\newpage

\addcontentsline{toc}{section}{Reading Items}
\section*{Reading Items}
To view the information individual table items select the "View" button in the item operations section of the desired item.\\
You will be brought to a Details page. From here you can select the export button to get the information for an individual item, or use the back button to return to the main table.\\

\addcontentsline{toc}{section}{Updating Items}
\section*{Updating Items}
To update a table item select the "Edit" button in the item operations section of the desired item.

\addcontentsline{toc}{subsection}{Updating Devices}
\subsection*{Updating Devices}
The update page for devices contains the same input fields as the create page for devices.
\begin{itemize}
    \item
    \textbf{Assignee Name} - The name of the person or section which the device is assigned.

    \item
    \textbf{Assignee Section} - If applicaple, the section to which an assignee belongs.

    \item
    \textbf{Location} - The physical location of the device, such as a cubicle or room number.

    \item
    \textbf{Asset Number} - The asset number given to the device.

    \item
    \textbf{Serial Number} - The serial number or service tag of the device.

    \item
    \textbf{Device Type} - The type of device. Laptop, printer, and monitor are all acceptable inputs.

    \item
    \textbf{Make} - The make of the device. Some examples would be: HP, DELL, and Lenovo.

    \item
    \textbf{Model} - The model of the device.

    \item
    \textbf{Assign Date} - The date on which the device was assigned.
\end{itemize}

You can see the current values already typed into the input fields.\\
To select a different assignee for the device, change the data in the Name and Section input fields to match the desired assignee.\\
The update date will automatically be set when when the "Update" button on the page is selected.

\addcontentsline{toc}{subsection}{Updating Assignees}
\subsection*{Updating Assignees}
\paragraph{} The update page for assignees contains the same input fields as the create page for assignees.
\begin{itemize}
    \item
    \textbf{Name} - The full name of the assignee or section.

    \item
    \textbf{Section} - If applicaple, the section to which the assignee belongs.
\end{itemize}

\paragraph{} If an assignee name is changed from "Jane A. Doe" to "Jane B. Doe" then all devices assigned to "Jane A. Doe" will now be listed as assigned to "Jane B. Doe".

\newpage

\addcontentsline{toc}{section}{Deleting Items}
\section*{Deleting Items}
\paragraph{} To delete an item Select the "Delete" button in Item Operations section of the desired item.\\
Upon selection the delete button will bring you to a page to confirm that you actually want to delete the item. If you were brought to the delete page accidentally then select "Return to table". If you do intend to delete the item from the table, select "Confirm Delete".

\addcontentsline{toc}{section}{Exporting Data}
\section*{Exporting Data}
\paragraph{} To export the contents of a table to a csv file, select the "Export" button located above the table.\\
Only the items visible on the current page will be exported. This means that if you select export on the first page of results for devices you will only export a maximum of 50 items.\\
\paragraph{} If you wish to export every item in a table then you must first select the "See All" button found below the search bar. You will be brought to a page displaying every single item in the table. From this page you can select the export button.\\
\paragraph{} It is also possible to export search results. If you search for devices assigned to "Jane Doe" and then select the export button you will be able to download a csv file containing information on all devices assigned to "Jane Doe".

\addcontentsline{toc}{section}{Importing Data}
To import data you need a csv file. When organizing your data ensure that none of the categories include:
\begin{itemize}
    \item
    unknown characters

    \item
    double quotes (")

    \item
    back slashes (\textbackslash)

    \item
    percentages (\%)

\end{itemize}

The csv file you wish to import must \textbf{not} include column headers, only input data.

\subsection*{Importing Devices}
\addcontentsline{toc}{subsection}{Importing Devices}
\paragraph{} The input data for devices must be written in this \textbf{exact order}:

\begin{table}[h]
    \small
        \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
        \hline
        \footnotesize
        Assignee & Location & Asset \# & Serial \# & Device Type & Make & Model & Assign Date & Update Date\\
        \hline
        \end{tabular}
\end{table}

\subsection*{Importing Assignees}
\addcontentsline{toc}{subsection}{Importing Assignees}
\paragraph{} The input data for assignees must be written in this \textbf{exact order}:

\begin{table}[h]
    \small
        \begin{tabular}{|c|c|}
        \hline
        \footnotesize
        Name & Section\\
        \hline
        \end{tabular}
\end{table}

\end{document}
