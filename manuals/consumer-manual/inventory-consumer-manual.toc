\babel@toc {nil}{}
\contentsline {section}{Database Structure}{2}{section*.1}%
\contentsline {subsection}{Devices}{2}{section*.2}%
\contentsline {subsubsection}{Devices Column Data}{2}{section*.3}%
\contentsline {subsection}{Assignees}{2}{section*.4}%
\contentsline {subsubsection}{Assignees Column Data}{2}{section*.5}%
\contentsline {section}{Navigation}{3}{section*.6}%
\contentsline {paragraph}{}{3}{section*.8}%
\contentsline {subsection}{Devices}{3}{section*.8}%
\contentsline {paragraph}{}{3}{section*.10}%
\contentsline {paragraph}{}{3}{section*.11}%
\contentsline {subsection}{Assignees}{3}{section*.11}%
\contentsline {section}{Searching}{4}{section*.12}%
\contentsline {subsection}{Devices Search}{4}{section*.13}%
\contentsline {subsection}{Assignees Search}{4}{section*.14}%
\contentsline {section}{Creating Items}{5}{section*.15}%
\contentsline {subsection}{Creating Devices}{5}{section*.16}%
\contentsline {subsection}{Creating Assignees}{5}{section*.17}%
\contentsline {section}{Reading Items}{6}{section*.18}%
\contentsline {section}{Updating Items}{6}{section*.19}%
\contentsline {subsection}{Updating Devices}{6}{section*.20}%
\contentsline {subsection}{Updating Assignees}{6}{section*.21}%
\contentsline {paragraph}{}{6}{section*.23}%
\contentsline {paragraph}{}{6}{section*.24}%
\contentsline {section}{Deleting Items}{7}{section*.24}%
\contentsline {paragraph}{}{7}{section*.26}%
\contentsline {section}{Exporting Data}{7}{section*.26}%
\contentsline {paragraph}{}{7}{section*.28}%
\contentsline {paragraph}{}{7}{section*.29}%
\contentsline {paragraph}{}{7}{section*.30}%
\contentsline {section}{Importing Data}{7}{section*.30}%
\contentsline {subsection}{Importing Devices}{7}{section*.31}%
\contentsline {paragraph}{}{7}{section*.32}%
\contentsline {subsection}{Importing Assignees}{7}{section*.33}%
\contentsline {paragraph}{}{7}{section*.34}%
