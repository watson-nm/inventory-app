\babel@toc {nil}{}
\contentsline {section}{Database Structure}{2}{section*.1}%
\contentsline {paragraph}{}{2}{section*.3}%
\contentsline {subsection}{Devices}{2}{section*.3}%
\contentsline {paragraph}{}{2}{section*.5}%
\contentsline {subsubsection}{Devices Column Data}{2}{section*.5}%
\contentsline {subsection}{Assignees}{2}{section*.6}%
\contentsline {subsubsection}{Assignees Column Data}{2}{section*.7}%
\contentsline {section}{Navigation}{3}{section*.8}%
\contentsline {paragraph}{}{3}{section*.10}%
\contentsline {subsection}{Devices}{3}{section*.10}%
\contentsline {paragraph}{}{3}{section*.12}%
\contentsline {paragraph}{}{3}{section*.13}%
\contentsline {paragraph}{}{3}{section*.14}%
\contentsline {subsection}{Assignees}{4}{section*.14}%
\contentsline {paragraph}{}{4}{section*.16}%
\contentsline {section}{Searching}{5}{section*.16}%
\contentsline {subsection}{Devices Search}{5}{section*.17}%
\contentsline {subsection}{Assignees Search}{5}{section*.18}%
\contentsline {section}{Creating Items}{6}{section*.19}%
\contentsline {paragraph}{}{6}{section*.21}%
\contentsline {paragraph}{}{6}{section*.22}%
\contentsline {subsection}{Creating Devices}{6}{section*.22}%
\contentsline {subsection}{Creating Assignees}{6}{section*.23}%
\contentsline {section}{Reading Items}{7}{section*.24}%
\contentsline {paragraph}{}{7}{section*.26}%
\contentsline {paragraph}{}{7}{section*.27}%
\contentsline {section}{Updating Items}{7}{section*.27}%
\contentsline {subsection}{Updating Devices}{7}{section*.28}%
\contentsline {paragraph}{}{7}{section*.30}%
\contentsline {subsection}{Updating Assignees}{7}{section*.30}%
\contentsline {paragraph}{}{7}{section*.32}%
\contentsline {paragraph}{}{8}{section*.33}%
\contentsline {section}{Deleting Items}{8}{section*.33}%
\contentsline {paragraph}{}{8}{section*.35}%
\contentsline {paragraph}{}{8}{section*.36}%
\contentsline {section}{Exporting Data}{8}{section*.36}%
\contentsline {paragraph}{}{8}{section*.38}%
\contentsline {paragraph}{}{8}{section*.39}%
\contentsline {paragraph}{}{8}{section*.40}%
\contentsline {section}{Importing Data}{8}{section*.40}%
\contentsline {paragraph}{}{8}{section*.42}%
\contentsline {paragraph}{}{8}{section*.43}%
\contentsline {paragraph}{}{8}{section*.44}%
\contentsline {paragraph}{}{9}{section*.45}%
\contentsline {subsection}{Importing Devices}{9}{section*.45}%
\contentsline {paragraph}{}{9}{section*.47}%
\contentsline {subsection}{Importing Assignees}{9}{section*.47}%
\contentsline {paragraph}{}{9}{section*.49}%
\contentsline {section}{Admin Information}{10}{section*.49}%
\contentsline {subsection}{Application File Structure}{10}{section*.50}%
\contentsline {subsection}{db.php}{11}{section*.51}%
\contentsline {paragraph}{}{11}{section*.53}%
\contentsline {subsection}{includes}{11}{section*.53}%
\contentsline {paragraph}{}{11}{section*.55}%
\contentsline {subsubsection}{assignees}{11}{section*.55}%
\contentsline {subsubsection}{devices}{11}{section*.56}%
\contentsline {subsubsection}{search-suggestions}{11}{section*.57}%
\contentsline {paragraph}{}{11}{section*.59}%
\contentsline {subsection}{Changing Database Structure}{12}{section*.59}%
\contentsline {paragraph}{}{12}{section*.61}%
\contentsline {paragraph}{}{12}{section*.62}%
\contentsline {paragraph}{}{12}{section*.63}%
\contentsline {subsubsection}{Changing Devices Table}{12}{lstnumber.-1.11}%
\contentsline {subsubsection}{Changing Assignees Table}{15}{lstnumber.-9.5}%
