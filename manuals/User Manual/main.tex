\documentclass[a4paper,10pt]{article}

\usepackage{ucs}
\usepackage[utf8]{inputenc}
\usepackage{babel}
\usepackage{fontenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage[export]{adjustbox}
\usepackage[hidelinks]{hyperref}
\usepackage[margin=0.6in]{geometry}
\usepackage{tabularx}
\usepackage{arev}
\usepackage[T1]{fontenc}
\usepackage[dvipsnames]{xcolor}
\usepackage{forest}
\usepackage{listings}

\NewDocumentCommand{\codeword}{v}{%
\texttt{\textcolor{blue}{#1}}%
}

\lstset{language=html,keywordstyle={\bfseries \color{blue}}}

\author{Nola Watson}
\title{Inventory Database Manager: Consumer Manual}

\begin{document}
\maketitle

\tableofcontents
\newpage

\addcontentsline{toc}{section}{Database Structure}
\section*{Database Structure}
\label{sec:Database-Structure}
\paragraph{} This application is designed to be an easy to use and navigate front end for an inventory database.\\
This database contains two connected tables: \textbf{devices} and \textbf{assignees}.

\addcontentsline{toc}{subsection}{Devices}
\subsection*{Devices}
\label{sec:Devices-Structure}
\paragraph{} The devices table contains most of the information in the database and has ten columns.

\begin{table}[h]
    \small
        \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
        \hline
        \footnotesize
        ID & Assignee & Location & Asset \# & Serial \# & Device Type & Make & Model & Assign Date & Update Date\\
        \hline
        \end{tabular}
\end{table}

\addcontentsline{toc}{subsubsection}{Devices Column Data}
\subsubsection*{Devices Column Data}
\label{sec:Devices-Column-Data}
\begin{itemize}
    \item
    \textbf{ID} - An automatically assigned unique id which identifies an item in a table.

    \item
    \textbf{Assignee} - The person, section, or location that a device is assigned to. In the database this is a numerical value matching an ID from the assignees table.

    \item
    \textbf{Location} - The physical location of a device; i.e. a cubicle or room number.

    \item
    \textbf{Asset \#} - The asset number assigned to a device.

    \item
    \textbf{Serial \#} - The serial number or service tag of a device; this is a unique value.

    \item
    \textbf{Device Type} - The type of a device (laptop, printer, monitor, etc.).

    \item
    \textbf{Make} - The make of a device.

    \item
    \textbf{Model} - The model of a device.

    \item
    \textbf{Assign Date} - The date when a device was given to an assignee.

    \item
    \textbf{Update Date} - The date when a table item was last updated.

\end{itemize}

\addcontentsline{toc}{subsection}{Assignees}
\subsection*{Assignees}
\label{sec:Assignees-Structure}
The assignees table contains the names of assigneees and, if applicaple, the sections they belong to.\\
There are only three columns in the assignees table.\\
\begin{table}[h]
    \centering
        \begin{tabular}{|c|c|c|}
        \hline
        ID & Name & Section \\
        \hline
        \end{tabular}
\end{table}

\addcontentsline{toc}{subsubsection}{Assignees Column Data}
\subsubsection*{Assignees Column Data}
\label{sec:Assignees-Column-Data}
\begin{itemize}
    \item
    \textbf{ID} - An automatically assigned unique id which identifies an item in a table. This is a numerical value linked to the assignees column of the devices table.

    \item
    \textbf{Name} - A person, section, or location.

    \item
    \textbf{Section} - The section an assignee belongs to; may help when differentiating assignee's with the same name.

\end{itemize}
\newpage

\addcontentsline{toc}{section}{Navigation}
\section*{Navigation}
\label{sec:Navigation}
\paragraph{} When first opening the inventory application users are greeted with the start page.\\
\includegraphics[width=0.5\textwidth, center]{start-page.JPG}

\addcontentsline{toc}{subsection}{Devices}
\subsection*{Devices}
\label{sec:Devices-Navigation}
\paragraph{} The device inventory section of the application displays the contents of the devices table, broken up into 50 results per page. There is a navigation bar at the bottom of the page which you can use to see more table items.

\paragraph{} Above the device information table there is:
\begin{itemize}
    \item
    A search bar (for help see \textbf{\hyperref[sec:Devices-Search]{Devices Search}})\\
    \includegraphics[height=1.2cm, center]{searchbar}

    \item
    A button which brings you to the device creation page (for help see \textbf{\hyperref[sec:Creating-Devices]{Creating Devices}})\\
    \includegraphics[height=1.2cm]{create-button.JPG}

    \item
    A button that brings you to the assignees page\\
    \includegraphics[height=1.2cm]{assignees-page.JPG}

    \item
    A button that allows you to see every device on a single page\\
    \includegraphics[height=1.2cm]{see-all-devices.JPG}

    \item
    An export button (for help see \textbf{\hyperref[sec:Exporting-Data]{Exporting Data}})\\
    \includegraphics[height=1.2cm]{export-button.JPG}

    \item
    An import button attached to a file selector (for help see \textbf{\hyperref[sec:Importing-Devices]{Importing Devices}})\\
    \includegraphics[height=1.2cm]{import-button.JPG}
    
\end{itemize}

\paragraph{} Below the table there is:
\begin{itemize}
    \item
    A navigation bar which allows you to view more table items\\
    \includegraphics[height=1.2cm]{nav-bar.JPG}
    
    \item
    A back button to return to the start page\\
    \includegraphics[height=1.2cm]{back-button.JPG}
    
    \item
    A link to the help page\\
    \includegraphics[height=1.3cm]{help.JPG}
    
\end{itemize}

\addcontentsline{toc}{subsection}{Assignees}
\subsection*{Assignees}
\label{Assignees-Navigation}
\paragraph{} To see the list of assignees select the "Assignees Page" button. Above the assignee information table there is:

\begin{itemize}
    \item
    A search bar (for help see \textbf{\hyperref[sec:Assignees-Search]{Assignees Search}})\\
    \includegraphics[height=1.2cm, center]{searchbar.JPG}
    
    \item
    A button which brings you to the assignee creation page (for help see \textbf{\hyperref[sec:Creating-Assignees]{Creating Assignees}})\\
    \includegraphics[height=1.2cm]{create-button.JPG}
    
    \item
    A button that brings you to the devices page\\
    \includegraphics[height=1.2cm]{devices-page.JPG}
    
    \item
    A button that allows you to see every assignee on a single page\\
    \includegraphics[height=1.2cm]{see-all-assignees.JPG}
    
    \item
    An export button (for help see \textbf{\hyperref[sec:Exporting-Data]{Exporting-Data}})\\
    \includegraphics[height=1.2cm]{export-button.JPG}
    
    \item
    An import button attached to a file selector (for help see \textbf{\hyperref[sec:Importing-Assignees]{Importing Assignees}})\\
    \includegraphics[height=1.2cm]{import-button.JPG}
    
\end{itemize}

Below the table there is:
\begin{itemize}
    \item
    A navigation bar which allows you to view more table items\\
    \includegraphics[height=1.2cm]{nav-small.JPG}
    
    \item
    A back button to return to the start page\\
    \includegraphics[height=1.2cm]{back-button.JPG}
    
    \item
    A link to the help page\\
    \includegraphics[height=1.3cm]{help.JPG}
    
\end{itemize}

\newpage

\addcontentsline{toc}{section}{Searching}
\section*{Searching}
\label{sec:Searching}
\includegraphics[width=16cm]{searchbar}

The search bar can be located at the top of the Devices, Assignees, and Search pages. The leftmost portion of the search bar is a drop-down menu, the center is a texbox where a user can enter a search term, and the rightmost portion is a button which will submit the query.\\

It is possible to perform a search using an incomplete search term. For example you may type the search term "Doe" when looking for all devices assigned to "Jane Doe".\\
\includegraphics[width=16cm]{search-exa}

\addcontentsline{toc}{subsection}{Devices Search}
\subsection*{Devices Search}
\label{sec:Devices-Search}

\begin{minipage}[t]{0.75\textwidth}
    The drop-down menu displays a list of searchable categories.

\begin{itemize}
    \item
    \textbf{ID} - Search for an item by using its unique table ID.

    \item
    \textbf{Assigned To} - Search for all items with a specific assignee.

    \item
    \textbf{Location} - Search for all items in a specif location.

    \item
    \textbf{Asset \#} - Search for an item by using its asset number.

    \item
    \textbf{Serial \#} - Search for an item by using its serial number/service tag.

    \item
    \textbf{Device} - Search for all instances of a certain device type.

    \item
    \textbf{Make} - Seach for all items of a certain make.

    \item
    \textbf{Model} - Search for all items of a certain model.

    \item
    \textbf{Assign Date} - Search for items by the date when they were assigned.

    \item
    \textbf{Update Date} - Search for items by when their entry was last updated.
\end{itemize}
When a category has been chosen and a search term typed into the text box select the Search button to be taken to a page with the search results.
\end{minipage}
\hfill
\begin{minipage}[t]{0.5\textwidth}
    \includegraphics[width=0.45\textwidth, valign=t]{search-dropdown-d}
\end{minipage}

\addcontentsline{toc}{subsection}{Assignees Search}
\subsection*{Assignees Search}
\label{sec:Assignees-Search}

\begin{minipage}[t]{0.8\textwidth}
The assignees drop-down only has three categories.
\begin{itemize}
    \item
    \textbf{ID} - Search for an assignee using a unique table ID.

    \item
    \textbf{Name} - Search for an assignee by name.

    \item
    \textbf{Section} - Search for any assignees in a particular section, if applicable.
\end{itemize}
As with the devices search when a category has been chosen and a search term supplied you can select the Search button to be brought to a page of search results.
\end{minipage}
\hfill
\begin{minipage}[t]{0.5\textwidth}
    \includegraphics[width=0.3\textwidth, valign=t]{search-dropdown-a}
\end{minipage}
\newpage

\addcontentsline{toc}{section}{Creating Items}
\section*{Creating Items}
\label{sec:Creating-Items}

\paragraph{} You can add new entries to the devices and assignees tables through the use of the "Create Entry" button located below the search bar.\\
\includegraphics[height=1.2cm]{create-button.JPG}

\paragraph{} When creating a new device, make sure that its assignee already exists first.

\addcontentsline{toc}{subsection}{Creating Devices}
\subsection*{Creating Devices}
\label{sec:Creating-Devices}
When creating a new device the following input fields are available:
\begin{itemize}
    \item
    \textbf{Assignee Name} - A required field, the name of the person or section which the device is to be assigned.

    \item
    \textbf{Assignee Section} - If applicable, the section to which an assignee belongs.

    \item
    \textbf{Location} - The physical location of the device, such as a cubicle or room number.

    \item
    \textbf{Asset Number} - The asset number given to the device.

    \item
    \textbf{Serial Number} - The serial number or service tag of the device.

    \item
    \textbf{Device Type} - The type of device. Laptop, printer, and monitor are all acceptable inputs.

    \item
    \textbf{Make} - The make of the device. Some examples would be: HP, DELL, and Lenovo.

    \item
    \textbf{Model} - The model of the device.

    \item
    \textbf{Assign Date} - The date on which the device is being assigned.
\end{itemize}

As you type, the page will provide suggestions for input based on data already in the table for: name, section, location, device type, make, and model.

\addcontentsline{toc}{subsection}{Creating Assignees}
\subsection*{Creating Assignees}
\label{sec:Creating-Assignees}
When creating a new assignee the following input fields are available:
\begin{itemize}
    \item
    \textbf{Name} - The full name of the assignee or section.

    \item
    \textbf{Section} - If applicable, the section to which the assignee belongs.
\end{itemize}

\newpage

\addcontentsline{toc}{section}{Reading Items}
\section*{Reading Items}
\label{sec:Reading-Items}
\paragraph{} To view the information individual table items select the "View" button in the item operations section of the desired item.\\
\includegraphics[height=2cm, center]{view.JPG}
\\
\paragraph{} You will be brought to a Details page.\\
\includegraphics[height=1.6cm, center]{read-exa.JPG}
\\
From here you can select the export button to get the information for an individual item, or use the back button to return to the main table.\\

\addcontentsline{toc}{section}{Updating Items}
\section*{Updating Items}
\label{sec:Updating-Items}
To update a table item select the "Edit" button in the item operations section of the desired item.\\
\includegraphics[height=2cm, center]{edit.jpg}
\\
\addcontentsline{toc}{subsection}{Updating Devices}
\subsection*{Updating Devices}
\label{sec:Updating-Devices}
The update page for devices contains the same input fields as the create page for devices.
\begin{itemize}
    \item
    \textbf{Assignee Name} - The name of the person or section which the device is assigned.

    \item
    \textbf{Assignee Section} - If applicable, the section to which an assignee belongs.

    \item
    \textbf{Location} - The physical location of the device, such as a cubicle or room number.

    \item
    \textbf{Asset Number} - The asset number given to the device.

    \item
    \textbf{Serial Number} - The serial number or service tag of the device.

    \item
    \textbf{Device Type} - The type of device. Laptop, printer, and monitor are all acceptable inputs.

    \item
    \textbf{Make} - The make of the device. Some examples would be: HP, DELL, and Lenovo.

    \item
    \textbf{Model} - The model of the device.

    \item
    \textbf{Assign Date} - The date on which the device was assigned.
\end{itemize}

\paragraph{} You can see the current values already typed into the input fields. You can reassign a device by changing the Name and Section to match a different assignee. The update date will automatically be set when when the "Update" button on the page is selected.

\addcontentsline{toc}{subsection}{Updating Assignees}
\subsection*{Updating Assignees}
\label{sec:Updating-Assignees}
\paragraph{} The update page for assignees contains the same input fields as the create page for assignees.
\begin{itemize}
    \item
    \textbf{Name} - The full name of the assignee or section.

    \item
    \textbf{Section} - If applicable, the section to which the assignee belongs.
\end{itemize}

\paragraph{} If an assignee name is changed from "Jane A. Doe" to "Jane B. Doe" then all devices assigned to "Jane A. Doe" will now be listed as assigned to "Jane B. Doe".

\addcontentsline{toc}{section}{Deleting Items}
\section*{Deleting Items}
\label{sec:Deleting-Items}

\paragraph{} To delete an item Select the "Delete" button in Item Operations section of the desired item.\\
\includegraphics[height=2cm, center]{delete.jpg}
\\
\paragraph{} Upon selection the delete button will bring you to a page to confirm that you actually want to delete the item. If you were brought to the delete page accidentally then select "Return to table". If you do intend to delete the item from the table, select "Confirm Delete".

\addcontentsline{toc}{section}{Exporting Data}
\section*{Exporting Data}
\label{sec:Exporting-Data}

\paragraph{} To export the contents of a table to a csv file, select the "Export" button located above the table.\\
\includegraphics[height=1.2cm]{export-button.JPG}
\\
Only the items \textbf{visible on the current page} will be exported. This means that if you select export on the first page of results for devices you will only export a maximum of 50 items.

\paragraph{} If you wish to export every item in a table then you must first select the "See All" button found below the search bar. You will be brought to a page displaying every single item in the table. From this page you can select the export button.

\paragraph{} It is also possible to export search results. If you search for devices assigned to "Jane Doe" and then select the export button you will be able to download a csv file containing information on all devices assigned to "Jane Doe".

\addcontentsline{toc}{section}{Importing Data}
\section*{Importing Data}
\label{sec:Importing-Data}

\paragraph{} Any data you wish to import must be written in a \textbf{csv file}.

\paragraph{} When organizing your data follow these guidelines:
\begin{itemize}
    \item
    Write all dates in mm-dd-yyyy format. (Example: 01-23-2020)
    
    \item
    Try and include data for every category, but empty spaces in the data \textbf{are} allowed.
    
    \item
    Do \textbf{not} include column headers

    \item
    Do \textbf{not} include unknown characters in your data

    \item
    Do \textbf{not} include double quotes (") in your data

    \item
    Do \textbf{not} include back slashes (\textbackslash) in your data

    \item
    Do \textbf{not} include percentages (\%) in your data

\end{itemize}

\paragraph{} Once you have created your csv file, select the file browser to upload it.\\
\includegraphics[height=1.2cm]{browse.JPG}
\\
\paragraph{} When you have uploaded your file, select the import button.\\
\includegraphics[height=1.2cm]{import-uploaded.JPG}
\\
\addcontentsline{toc}{subsection}{Importing Devices}
\subsection*{Importing Devices}
\label{sec:Importing-Devices}

\paragraph{} The columns of input data for devices must be arranged in this \textbf{exact order}:

\begin{table}[h]
    \small
        \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
        \hline
        \footnotesize
        Assignee & Location & Asset \# & Serial \# & Device Type & Make & Model & Assign Date & Update Date\\
        \hline
        \end{tabular}
\end{table}

\addcontentsline{toc}{subsection}{Importing Assignees}
\subsection*{Importing Assignees}
\label{sec:Importing-Assignees}

\paragraph{} The columns of input data for assignees must be arranged in this \textbf{exact order}:

\begin{table}[h]
    \small
        \begin{tabular}{|c|c|}
        \hline
        \footnotesize
        Name & Section\\
        \hline
        \end{tabular}
\end{table}

\newpage

\addcontentsline{toc}{section}{Admin Information}
\section*{Admin Information}

\addcontentsline{toc}{subsection}{Application File Structure}
\subsection*{Application File Structure}

{
\footnotesize
\begin{forest}
  for tree={
    font=\ttfamily,
    text=white,
    if level=0{fill=teal}
        {if level=1{fill=ForestGreen}
            {if level=2{fill=olive}
                {if level=3{fill=SeaGreen}}}},
    rounded corners=4pt,
    grow'=0,
    child anchor=west,
    parent anchor=south,
    anchor=west,
    calign=first,
    edge={ForestGreen,rounded corners,line width=1pt},
    edge path={
      \noexpand\path [draw, \forestoption{edge}]
      (!u.south west) +(7.5pt,0) |- (.child anchor)\forestoption{edge label};
    },
    before typesetting nodes={
      if n=1
        {insert before={[,phantom]}}
        {}
    },
    fit=band,
    before computing xy={l=15pt},
  }
[/
    [includes
        [assignees
            [assignees.php]
            [create-assignee.php]
            [delete-assignee.php]
            [read-assignee.php]
            [search-assignees.php]
            [see-all.php]
            [update-assignee.php]
        ]
        [devices
            [create.php]
            [delete.php]
            [home.php]
            [read.php]
            [search.php]
            [see-all.php]
            [update.php]
        ]
        [search-suggestions
            [dev-type-search.php]
            [location-search.php]
            [make-search.php]
            [model-search.php]
            [name-search.php]
            [section-search.php]
        ]
    ]
    [logs]
    [uploads]
    [db.php.]
    [export.php]
    [footer.php]
    [header.php]
    [help.php]
    [import-assignees.php]
    [import-devices.php]
    [index.php]
]
\end{forest}
}

\newpage

\addcontentsline{toc}{subsection}{db.php}
\subsection*{db.php}
\paragraph{} This file contains the code for accessing your mysql database. You must make sure that you set \codeword{$host} to the correct server name, \codeword{$user} to a user with all database privileges, \codeword{$pass} to the user's password, and \codeword{$database} to the database you are storing data in. You can also change the number of results you see on the devices and assignees pages with the variable \codeword{$results_per_page}.

\addcontentsline{toc}{subsection}{includes}
\subsection*{includes}
\paragraph{} This directory contains all of the files for the basic CRUD (Create, Read, Update, Delete) operations for the assignees and devices tables, as well as search functions.

\addcontentsline{toc}{subsubsection}{assignees}
\subsubsection*{assignees}
\begin{itemize}
    \item
    \textbf{assignees.php} - Main page for viewing the contents of the assignees table.
    
    \item
    \textbf{create-assignee.php} - Page for inputting data to create a new assignee.
    
    \item
    \textbf{delete-assignee.php} - Page for deleting an assignee from the table.
    
    \item
    \textbf{read-assignee.php} - Page for viewing the information of an individual assignee.
    
    \item
    \textbf{search-assignees.php} - Page which searches for assignees and displays results in a table.
    
    \item
    \textbf{see-all.php} - Page for viewing the contents of the assignees table all at once.
    
    \item
    \textbf{update-assignee.php} - Page for changing the information of an assignee.
    
\end{itemize}

\addcontentsline{toc}{subsubsection}{devices}
\subsubsection*{devices}
\begin{itemize}
    \item
    \textbf{create.php} - Page for inputting data to create new device.
    
    \item
    \textbf{delete.php} - Page for deleting a device from the table.
    
    \item
    \textbf{home.php} - Main page for viewing the contents of the devices table.
    
    \item
    \textbf{read.php} - Page for viewing the information of an individual device.
    
    \item
    \textbf{search.php} - Page which searches for devices and displays results in a table.
    
    \item
    \textbf{see-all.php} - Page for viewing the contents of the devices table all at once.
    
    \item
    \textbf{update.php} - Page for chaning the information of a device.
\end{itemize}

\addcontentsline{toc}{subsubsection}{search-suggestions}
\subsubsection*{search-suggestions}
\paragraph{} The files in this directory are used when creating or updating table items. They contain the php code for getting items from the database so that when users are typing they are able to see suggestions from the database.
\begin{itemize}
    \item
    \textbf{dev-type-search.php} - Finds all the distinct device types in the database.
    
    \item
    \textbf{location-search.php} - Finds all the distinct locations in the database
    
    \item
    \textbf{make-search.php} - Finds all the distinct device makes in the database.
    
    \item
    \textbf{model-search.php} - Finds all the distinct device models in the database.
    
    \item
    \textbf{name-search.php} - Finds all the distinct assignee names in the database.
    
    \item
    \textbf{section-search.php} - Finds all the distinct assignee sections in the database.
\end{itemize}

\newpage

\addcontentsline{toc}{subsection}{Changing Database Structure}
\subsection*{Changing Database Structure}
\paragraph{} The easiest way to edit the database you are using would be with phpmyadmin or a similar tool. If you are unable to use such a tool, use mysql commands to insert and drop table columns to suit your needs. The default structures of the tables are:
\begin{table}[h]
    \centering
    \caption{Devices Table}
        \begin{tabular}{|c|c|c|c|}
        \hline
        Column Name & Type & Default & Extra \\
        \hline
        dID & int(11) & none & primary, auto increment\\
        \hline
        assignee & int(11) & null & foreign key\\
        \hline
        location & varchar(30) & null & \\
        \hline
        asset\_num & varchar(25) & null & \\
        \hline
        serial\_num & varchar(25) & null & unique \\
        \hline
        dev\_type & varchar(25) & null & \\
        \hline
        make & varchar(25) & null & \\
        \hline
        model & varchar(50) & null & \\
        \hline
        assign\_date & date & null & \\
        \hline
        update\_date & date & null & \\
        \hline
        \end{tabular}
\end{table}

\begin{table}[h]
    \centering
    \caption{Assignees Table}
        \begin{tabular}{|c|c|c|c|}
        \hline
        Column Name & Type & Default & Extra \\
        \hline
        aID & int(11) & none & primary, auto increment\\
        \hline
        name & varchar(55) & null & \\
        \hline
        section & varchar(30) & null & \\
        \hline
        \end{tabular}
\end{table}

\paragraph{} You can use mysql commands to alter the tables.
\begin{itemize}
    \item
    Insert new column as first column.
    \begin{itemize}
        \item
        Syntax: \codeword{ALTER TABLE table ADD COLUMN column_name column_type FIRST;}
        
        \item
        Example: \codeword{ALTER TABLE devices ADD COLUMN new_col VARCHAR(25) FIRST;}
    \end{itemize}

    \item
    Insert new column after an existing column.
    \begin{itemize}
        \item
        Syntax: \codeword{ALTER TABLE table ADD COLUMN column_name column_type AFTER existing_column;}
        
        \item
        Example: \codeword{ALTER TABLE assignees ADD COLUMN new_col VARCHAR(25) AFTER name;}
    \end{itemize}
    
    \item
    Insert new column as last column.
    \begin{itemize}
        \item
        Syntax: \codeword{ALTER TABLE table ADD COLUMN column_name column_type;}
        
        \item
        Example: \codeword{ALTER TABLE devices ADD COLUMN new_col VARCHAR(25);}
    \end{itemize}
\end{itemize}

\paragraph{} Once you have inserted your new column, you must change the php files so that the data in those columns is visible. Sections with code you will need to change are labeled. Depending on the section the format will be one of the following:
\begin{lstlisting}
# Section Name
#####
    Relevant code
#####

-------

<!-- Section Name -->
<!-- ##### -->
    Relevant code
<!-- ##### -->
\end{lstlisting}

% FIXME make sure that you include import files

\addcontentsline{toc}{subsubsection}{Changing Devices Table}
\subsubsection*{Changing Devices Table}

\begin{itemize}
    \item
    \textbf{create.php}
    \begin{itemize}
        \item
        Add new variable in section "Input From Form"
        \begin{itemize}
            \item
            \codeword{$new_variable = $_POST['new_variable'];}
        \end{itemize}
        
        \item
        Add new column name and matching variable to command in section "Insert Command"
        \begin{itemize}
            \item
            Add \codeword{new_variable} to \codeword{devices()} and add \codeword{{$new_variable}'} to \codeword{VALUES()}
        \end{itemize}
        
        \item
        Add new form group to section "Input Form"
        \begin{itemize}
        \footnotesize
        \item
        \begin{lstlisting}
<div class="form-group">
    <label for="new_variable" class="form-label"> New Variable Label</label>
    <input type="text" name="new_variable" id="new_variable" class="form-control" />
</div>
        \end{lstlisting}
        \end{itemize}
    \end{itemize}
    
    \item
    \textbf{home.php}
    \begin{itemize}
        \item
        Add new option in section "Search Categories"
        \begin{itemize}
            \item
            \begin{lstlisting}
<option value="new_variable">New Variable</option>
            \end{lstlisting}
        \end{itemize}
        
        \item
        Add new column in section "Table Head"
        \begin{itemize}
            \item
            \begin{lstlisting}
<th scope="col">New Variable</th>
            \end{lstlisting}
        \end{itemize}
        
        \item
        Add new variable to section "Get From Database"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row['new_variable'];}
        \end{itemize}
        
        \item
        Add new variable to "Data Array"
        \begin{itemize}
            \item
            Add \codeword($new_variable) to \codeword{array()}
        \end{itemize}
        
        \item
        Add new echo to "Echo Table Contents"
        \begin{itemize}
            \item
            \codeword{echo "<td >{$new_variable}</td>";}
        \end{itemize}
    \end{itemize}
    
    \item
    \textbf{read.php}
    \begin{itemize}
        \item
        Add new column in section "Table Head"
        \begin{itemize}
            \item
            \begin{lstlisting}
<th scope="col">New Variable</th>
            \end{lstlisting}
        \end{itemize}
        
        \item
        Add new variable to section "Get From Database"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row['new_variable'];}
        \end{itemize}
        
        \item
        Add new variable to "Data Array"
        \begin{itemize}
            \item
            Add \codeword($new_variable) to \codeword{array()}
        \end{itemize}
        
        \item
        Add new echo to "Echo Table Contents"
        \begin{itemize}
            \item
            \codeword{echo "<td >{$new_variable}</td>";}
        \end{itemize}
    \end{itemize}
    
    \item
    \textbf{search.php}
    \begin{itemize}
        \item
        Add new option in section "Search Categories"
        \begin{itemize}
            \item
            \begin{lstlisting}
<option value="new_variable">New Variable</option>
            \end{lstlisting}
        \end{itemize}
        
        \item
        Add new column in section "Table Head"
        \begin{itemize}
            \item
            \begin{lstlisting}
<th scope="col">New Variable</th>
            \end{lstlisting}
        \end{itemize}
        
        \item
        Add new variable to section "Name Search: Get From Database"
        \begin{itemize}
            \item
            \codeword{$new_variable = $result['new_variable'];}
        \end{itemize}
        
        \item
        Add new variable to "Name Search: Data Array"
        \begin{itemize}
            \item
            Add \codeword($new_variable) to \codeword{array()}
        \end{itemize}
        
        \item
        Add new echo to "Name Search: Echo Table Contents"
        \begin{itemize}
            \item
            \codeword{echo "<td >{$new_variable}</td>";}
        \end{itemize}
        
        \item
        Add new variable to section "Other: Get From Database"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row['new_variable'];}
        \end{itemize}
        
        \item
        Add new variable to "Other: Data Array"
        \begin{itemize}
            \item
            Add \codeword($new_variable) to \codeword{array()}
        \end{itemize}
        
        \item
        Add new echo to "Other: Echo Table Contents"
        \begin{itemize}
            \item
            \codeword{echo "<td >{$new_variable}</td>";}
        \end{itemize}
    \end{itemize}
    
    \item
    \textbf{see-all.php}
    \begin{itemize}
        \item
        Add new column in section "Table Head"
        \begin{itemize}
            \item
            \begin{lstlisting}
<th scope="col">New Variable</th>
            \end{lstlisting}
        \end{itemize}
        
        \item
        Add new variable to section "Get From Database"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row['new_variable'];}
        \end{itemize}
        
        \item
        Add new variable to "Data Array"
        \begin{itemize}
            \item
            Add \codeword($new_variable) to \codeword{array()}
        \end{itemize}
        
        \item
        Add new echo to "Echo Table Contents"
        \begin{itemize}
            \item
            \codeword{echo "<td >{$new_variable}</td>";}
        \end{itemize}
    \end{itemize}
    
    \item
    \textbf{update.php}
    \begin{itemize}
        \item
        Add new variable to section "Get From Database"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row['new_variable'];}
        \end{itemize}
        
        \item
        Add new variable to section "Input From Form"
        \begin{itemize}
            \item
            \codeword{$new_variable = $_POST['new_variable'];}
        \end{itemize}
        
        \item
        Add new column name and matching variable to command in section "Update Command"
        \begin{itemize}
            \item
            Add \codeword{$new_variable = '{$new_variable}'} to the command
        \end{itemize}
        
        \item
        Add new form group to section "Input Form"
        \begin{lstlisting}
<div class="form-group">
    <label for="new_variable" class="form-label"> New Variable Label</label>
    <input type="text" name="new_variable" id="new_variable" class="form-control"
    value="<?php echo $new_variable ?>">
</div>
        \end{lstlisting}
    \end{itemize}

    \item
    \textbf{import-devices.php}
    \begin{itemize}
        \item
        Add new variable to section "Reading From File"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row[array_value];}
        \end{itemize}

        \item
        Add new column name and matching variable to command in section "Insert Command"
        \begin{itemize}
            \item
            Add \codeword{new_variable} to \codeword{devices()} and \codeword{'$new_variable'} to \codeword{VALUES()}
        \end{itemize}
    \end{itemize}
\end{itemize}

\newpage

\addcontentsline{toc}{subsubsection}{Changing Assignees Table}
\subsubsection*{Changing Assignees Table}
\begin{itemize}
    \item
    \textbf{assignees.php}
    \begin{itemize}
        \item
        Add new option in section "Search Categories"
        \begin{itemize}
            \item
            \begin{lstlisting}
<option value="new_variable">New Variable</option>
            \end{lstlisting}
        \end{itemize}
        
        \item
        Add new column in section "Table Head"
        \begin{itemize}
            \item
            \begin{lstlisting}
<th scope="col">New Variable</th>
            \end{lstlisting}
        \end{itemize}
        
        \item
        Add new variable to section "Get From Database"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row['new_variable'];}
        \end{itemize}
        
        \item
        Add new variable to "Data Array"
        \begin{itemize}
            \item
            Add \codeword($new_variable) to \codeword{array()}
        \end{itemize}
        
        \item
        Add new echo to "Echo Table Contents"
        \begin{itemize}
            \item
            \codeword{echo "<td >{$new_variable}</td>";}
        \end{itemize}
    \end{itemize}
    
    \item
    \textbf{create-assignee.php}
    \begin{itemize}
        \item
        Add new variable in section "Input From Form"
        \begin{itemize}
            \item
            \codeword{$new_variable = $_POST['new_variable'];}
        \end{itemize}

        \item
        Add new column name and matching variable to command in section "Insert Command"
        \begin{itemize}
            \item
            Add \codeword{new_variable} to \codeword{assignees()} and add \codeword{{$new_variable}'} to \codeword{VALUES()}
        \end{itemize}

        \item
        Add new form group in section "Input Form"
        \begin{itemize}
        \footnotesize
        \item
        \begin{lstlisting}
<div class="form-group">
    <label for="new_variable" class="form-label"> New Variable Label</label>
    <input type="text" name="new_variable" id="new_variable" class="form-control" />
</div>
        \end{lstlisting}
        \end{itemize}
    \end{itemize}
    
    \item
    \textbf{read-assignee.php}
    \begin{itemize}
        \item
        Add new column in section "Table Head"
        \begin{itemize}
            \item
            \begin{lstlisting}
<th scope="col">New Variable</th>
            \end{lstlisting}
        \end{itemize}

        \item
        Add new variable in section "Get From Database"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row['new_variable'];}
        \end{itemize}

        \item
        Add new variable to "Data Array"
        \begin{itemize}
            \item
            Add \codeword($new_variable) to \codeword{array()}
        \end{itemize}

        \item
        Add new echo to "Echo Table Contents"
        \begin{itemize}
            \item
            \codeword{echo "<td >{$new_variable}</td>";}
        \end{itemize}
    \end{itemize}
    
    \item
    \textbf{search-assignees.php}
    \begin{itemize}
        \item
        Add new option in section "Search Categories"
        \begin{itemize}
            \item
            \begin{lstlisting}
<option value="new_variable">New Variable</option>
            \end{lstlisting}
        \end{itemize}

        \item
        Add new column in section "Table Head"
        \begin{itemize}
            \item
            \begin{lstlisting}
<th scope="col">New Variable</th>
            \end{lstlisting}
        \end{itemize}

        \item
        Add new variable in section "Get From Database"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row['new_variable'];}
        \end{itemize}

        \item
        Add new variable to "Data Array"
        \begin{itemize}
            \item
            Add \codeword($new_variable) to \codeword{array()}
        \end{itemize}

        \item
        Add new echo to "Echo Table Contents"
        \begin{itemize}
            \item
            \codeword{echo "<td >{$new_variable}</td>";}
        \end{itemize}
    \end{itemize}
    
    \item
    \textbf{update-assignee.php}
    \begin{itemize}
        \item
        Add new variable to section "Get From Database"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row['new_variable'];}
        \end{itemize}

        \item
        Add new variable to section "Input From Form"
        \begin{itemize}
            \item
            \codeword{$new_variable = $_POST['new_variable'];}
        \end{itemize}

        \item
        Add new column name and matching variable to command in section "Update Command"
        \begin{itemize}
            \item
            Add \codeword{$new_variable = '{$new_variable}'} to the command
        \end{itemize}

        \item
        Add new form group to section "Input Form"
        \begin{itemize}
        \footnotesize
        \item
        \begin{lstlisting}
<div class="form-group">
    <label for="new_variable" class="form-label"> New Variable Label</label>
    <input type="text" name="new_variable" id="new_variable" class="form-control" />
</div>
        \end{lstlisting}
        \end{itemize}
    \end{itemize}

    \item
    \textbf{import-assignees.php}
    \begin{itemize}
        \item
        Add new variable to section "Reading From File"
        \begin{itemize}
            \item
            \codeword{$new_variable = $row[array_value];}
        \end{itemize}

        \item
        Add new column name and matching variable to command in section "Insert Command"
        \begin{itemize}
            \item
            Add \codeword{new_variable} to \codeword{devices()} and \codeword{'$new_variable'} to \codeword{VALUES()}
        \end{itemize}
    \end{itemize}
\end{itemize}
\end{document}
